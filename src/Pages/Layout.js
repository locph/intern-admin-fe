import React from 'react';
import { Layout } from 'antd';

import './Layout.scss';
import SideBar from '../Components/SideBar/SideBar';
import Header from '../Components/Header/Header';

export default function PageLayout(props) {
  return (
    <Layout.Content className="container layout">
      <SideBar />
      <div className="main-app">
        <Header {...props} />
        {props.children}
      </div>
    </Layout.Content>
  );
}
