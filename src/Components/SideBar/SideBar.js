import React from 'react';
import { NavLink } from 'react-router-dom';
import { Space } from 'antd';
import {
  // ShoppingCartOutlined,
  // BarChartOutlined,
  UnorderedListOutlined,
} from '@ant-design/icons';

import './SideBar.scss';
import logo from '../../assets/images/logo.svg';
// import overview from '../../assets/images/overview-dark.svg';
// import orders from '../../assets/images/orders-dark.svg';
// import products from '../../assets/images/products-orange.svg';

const navbar = [
  // { icon: <BarChartOutlined />, link: '/overview', title: 'Overview' },
  // { icon: <ShoppingCartOutlined />, link: '/orders', title: 'Orders' },
  { icon: <UnorderedListOutlined />, link: '/products', title: 'Products' },
];

export default function SideBar() {
  return (
    <div className="sidebar">
      <div className="logo">
        <img alt="logo" src={logo} />
      </div>
      {navbar.map((nav) => (
        <div className="navbar" key={nav.link}>
          <NavLink to={nav.link}>
            <Space size={20}>
              {nav.icon}
              <span className="Text-Style-8">{nav.title}</span>
            </Space>
          </NavLink>
        </div>
      ))}
    </div>
  );
}
