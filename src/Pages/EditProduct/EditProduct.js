import React, { useEffect, useState } from 'react';
import {
  Button,
  Col,
  Form,
  Input,
  InputNumber,
  Modal,
  Row,
  Select,
  Tag,
} from 'antd';
import { Link } from 'react-router-dom';

// import './EditProduct.scss';
import Layout from '../Layout';
import { brands, categorires, colors, size } from '../AddProduct/Categories';
import axios from '../../Util/axios';

export default function EditProduct(props) {
  const [form] = Form.useForm();
  const [loading] = useState(false);
  const [files, setFiles] = useState([]);

  useEffect(() => {
    axios
      .get(`/admin/product/${props.match.params.productId}`)
      .then((res) => {
        form.setFieldsValue(res.data);
        setFiles(res.data.photos);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [props.match.params.productId, form]);

  const submitFormHandler = (form) => {
    axios
      .put(`/admin/product/${props.match.params.productId}`, form)
      .then((res) => {
        Modal.success({
          title: 'Success!',
          content: 'Your product has been edited!',
          onOk() {
            props.history.push('/');
          },
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <Layout title="Edit product" subTitle="Products / Edit product">
      <Row className="photos">
        <Col span={4} className="label">
          <label htmlFor="photo">PHOTOS</label>
        </Col>
        <Col span={16}>
          <Row gutter={[6, 6]}>
            {files.map((file) => (
              <Col span={6} key={file} className="img-preview">
                <img
                  src={`http://localhost:4808/images/${file}`}
                  alt={file.name}
                />
              </Col>
            ))}
          </Row>
        </Col>
      </Row>

      <Form
        name="add-product"
        className="form-add"
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 16 }}
        form={form}
        onFinish={submitFormHandler}
      >
        <Form.Item label="NAME" name="name">
          <Input size="large" />
        </Form.Item>

        <Form.Item label="CATEGORIES" name="categories">
          <Select
            mode="multiple"
            size="large"
            options={categorires}
            showArrow={true}
          />
        </Form.Item>

        <Form.Item label="BRAND" name="brand">
          <Select size="large" options={brands} showArrow={true} />
        </Form.Item>

        <Form.Item label="PRICE ($)" name="price">
          <InputNumber size="large" min={0} step={0.01} />
        </Form.Item>

        <Form.Item label="SIZE" name="size">
          <Select
            mode="multiple"
            size="large"
            options={size}
            showArrow={true}
          />
        </Form.Item>

        <Form.Item label="COLOR" name="color">
          <Select
            mode="multiple"
            size="large"
            options={colors}
            showArrow={true}
            tagRender={(props) => (
              <Tag
                {...props}
                color={props.value === 'white' ? 'default' : props.value}
              >
                {props.label}
              </Tag>
            )}
          />
        </Form.Item>

        <Form.Item label="QUANTITY" name="quantity">
          <InputNumber size="large" min={0} step={1} />
        </Form.Item>

        <Form.Item label="DESCRIPTION" name="description">
          <Input.TextArea size="large" autoSize={{ minRows: 2 }} />
        </Form.Item>

        <Form.Item className="btn-group" wrapperCol={{ offset: 4, span: 16 }}>
          <Link to="/products">
            <Button
              type="default"
              className="secondary-btn"
              size="large"
              loading={loading}
            >
              Cancel
            </Button>
          </Link>
          <Button
            type="primary"
            className="primary-btn"
            size="large"
            htmlType="submit"
            loading={loading}
          >
            Submit
          </Button>
        </Form.Item>
      </Form>
    </Layout>
  );
}
