import React, { useCallback, useEffect, useState } from 'react';
import { Button, Dropdown, Input, Menu, Modal, Table } from 'antd';
import {
  DownloadOutlined,
  PlusOutlined,
  SearchOutlined,
  EditFilled,
  DeleteFilled,
  CaretDownOutlined,
} from '@ant-design/icons';
import { Link } from 'react-router-dom';
import moment from 'moment';
import 'moment/locale/vi';

import './Products.scss';
import Layout from '../Layout';
import axios from '../../Util/axios';
import { categorires } from '../AddProduct/Categories';

moment.locale('vi');

export default function Products(props) {
  const [products, setProducts] = useState([]);
  const [total, setTotal] = useState(0);
  const [page, setPage] = useState(1);
  const [sortBy, setSortBy] = useState('date');
  const [name, setName] = useState('');
  const [loading, setLoading] = useState(false);

  const fetchProducts = useCallback((page, sortBy, name) => {
    axios
      .get(`/admin/products?page=${page}&sortBy=${sortBy}&name=${name}`)
      .then((res) => {
        setProducts(
          res.data.products.map((product) => ({ ...product, key: product._id }))
        );
        setTotal(res.data.totalProduct);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  useEffect(() => {
    fetchProducts(page, sortBy, name);
  }, [page, sortBy, name, fetchProducts]);

  const popupErrorHandler = () => {
    Modal.error({
      title: 'Something went wrong!',
      content: 'Please check you action and try again later!',
      okButtonProps: {
        size: 'large',
        danger: true,
      },
    });
  };

  const deleteProductHandler = (productId) => {
    Modal.confirm({
      title: 'Do you Want to delete these products?',
      content: 'These products will be delete from database.',
      onOk() {
        setLoading(true);

        axios
          .delete(`/admin/product/${productId}`)
          .then((res) => {
            setLoading(false);
            // TODO: for fetch again product from db
            fetchProducts(page, sortBy, name);
          })
          .catch((err) => {
            console.log(err);
            popupErrorHandler();
            setLoading(false);
          });
      },
      okButtonProps: {
        danger: true,
      },
      okText: 'Remove',
    });
  };

  const columns = [
    {
      title: 'PRODUCTS',
      dataIndex: 'name',
      key: 'name',
      render: (text, record) => {
        return (
          <div className="table-item">
            <img
              src={`http://localhost:4808/images/${record.photos[0]}`}
              alt={text}
            />
            <div className="info">
              <h2>{record.name}</h2>
              <span>
                {
                  categorires.find((c) => c.value === record.categories[0])
                    .label
                }
              </span>
            </div>
          </div>
        );
      },
    },
    {
      title: 'SOLD',
      dataIndex: 'quantity',
      key: 'quantity',
      render: (text) => <span className="text">{text}</span>,
    },
    {
      title: 'DATE ADDED',
      dataIndex: 'createdAt',
      key: 'createdAt',
      render: (text) => (
        <span className="text">{moment(text).format('L')}</span>
      ),
    },
    {
      title: 'PROFIT ($)',
      dataIndex: 'price',
      key: 'price',
      render: (text) => <span className="text">{text.toFixed(2)}</span>,
    },
    {
      title: '',
      key: 'action',
      render: (record) => (
        <Dropdown
          overlay={
            <Menu>
              <Menu.Item
                icon={<EditFilled />}
                onClick={() => {
                  props.history.push(`/products/edit/${record._id}`);
                }}
              >
                Edit
              </Menu.Item>
              <Menu.Item
                icon={<DeleteFilled />}
                onClick={() => {
                  deleteProductHandler(record._id);
                }}
              >
                Remove
              </Menu.Item>
            </Menu>
          }
          trigger={['click']}
        >
          <Button type="link" size="large">
            Actions <CaretDownOutlined />
          </Button>
        </Dropdown>
      ),
    },
  ];

  return (
    <Layout title="Products">
      <div className="products">
        <div className="ulity">
          <div className="left">
            <label htmlFor="sort">SORT BY</label>
            <select value={sortBy} onChange={(e) => setSortBy(e.target.value)}>
              <option value="createAt">Date added</option>
              <option value="name">Name</option>
              <option value="price">Profit</option>
              <option value="quantity">Quantity</option>
            </select>
          </div>

          <div className="right">
            <Input
              size="large"
              className="input-primary"
              prefix={<SearchOutlined />}
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
            <Link to="/products/add">
              <Button
                type="primary"
                icon={<PlusOutlined />}
                className="primary-btn"
                size="large"
              >
                Add product
              </Button>
            </Link>
            <Button
              className="secondary-btn"
              icon={<DownloadOutlined />}
              size="large"
            >
              Export
            </Button>
          </div>
        </div>

        <Table
          loading={loading}
          columns={columns}
          dataSource={products}
          pagination={{
            pageSize: 10,
            total,
            showSizeChanger: false,
            onChange: (page) => {
              setPage(page);
            },
          }}
        />
      </div>
    </Layout>
  );
}
