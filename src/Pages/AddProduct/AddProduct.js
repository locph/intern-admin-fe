import React, { useRef, useState } from 'react';
import { CloseOutlined } from '@ant-design/icons';
import {
  Button,
  Col,
  Form,
  Input,
  InputNumber,
  Modal,
  Row,
  Select,
  Tag,
} from 'antd';
import { Link } from 'react-router-dom';

import addImg from '../../assets/images/add.svg';
import Layout from '../Layout';
import './AddProduct.scss';
import axios from '../../Util/axios';

const { categorires, brands, colors, size } = require('./Categories');

export default function AddProduct(props) {
  const [files, setFiles] = useState([]);
  const [loading, setLoading] = useState(false);
  const photoRef = useRef();

  const submitFormHandler = (form) => {
    setLoading(true);

    const formData = new FormData();

    for (const key in form) {
      formData.append(key, form[key]);
    }

    files.forEach((file) => {
      formData.append('file[]', file);
    });

    axios
      .post('/admin/product', formData, {
        headers: {
          'Content-type': 'multipart/form-data',
        },
      })
      .then((res) => {
        Modal.success({
          title: 'Success!',
          content: 'Your product has been added into database!',
          onOk() {
            props.history.push('/');
          },
        });
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
        openErrorModal();
      });
  };

  const changeImagesHandler = (e) => {
    const newFiles = [...files];

    for (let i = 0; i < e.target.files.length; i++) {
      const isExist = newFiles.find(
        (file) => file.name === e.target.files[i].name
      );

      if (!isExist) {
        newFiles.push(e.target.files[i]);
      }
    }

    setFiles(newFiles.slice(0, 8));
  };

  const clickPhotoRef = () => {
    photoRef.current.click();
  };

  const deleteImgHandler = (file) => {
    setFiles((pre) => pre.filter((oldFile) => oldFile.name !== file.name));
  };

  const openErrorModal = () => {
    Modal.error({
      title: 'Something went wrong.',
      content: 'Please try again later.',
    });
  };

  return (
    <Layout title="Add product" subTitle="Products / Add product">
      <Row className="photos">
        <Col span={4} className="label">
          <label htmlFor="photo">PHOTOS</label>
        </Col>
        <Col span={16}>
          <input
            type="file"
            ref={photoRef}
            multiple
            value={[]}
            onChange={changeImagesHandler}
            accept="jpg, jpeg, png"
          />
          <Row gutter={[6, 6]}>
            {files.map((file) => (
              <Col span={6} key={file.name} className="img-preview">
                <img src={URL.createObjectURL(file)} alt={file.name} />
                <Button
                  type="text"
                  className="close-btn"
                  icon={<CloseOutlined />}
                  onClick={() => deleteImgHandler(file)}
                />
              </Col>
            ))}

            {Array.from(
              Array(4 - files.length + (files.length < 4 ? 0 : 4)),
              (v, i) => i
            ).map((replace) => (
              <Col span={6} key={replace}>
                <div className="click-add-img" onClick={clickPhotoRef}>
                  <img src={addImg} alt="add" />
                  <h2>Add photo</h2>
                </div>
              </Col>
            ))}

            <Col span={24}>
              <h2>
                You can add up to 8 photos. The 1st photo will be set as cover
                (main photo).
              </h2>
            </Col>
          </Row>
        </Col>
      </Row>

      <Form
        name="add-product"
        className="form-add"
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 16 }}
        onFinish={submitFormHandler}
      >
        <Form.Item label="NAME" name="name">
          <Input size="large" />
        </Form.Item>

        <Form.Item label="CATEGORIES" name="categories">
          <Select
            mode="multiple"
            size="large"
            options={categorires}
            showArrow={true}
          />
        </Form.Item>

        <Form.Item label="BRAND" name="brand">
          <Select size="large" options={brands} showArrow={true} />
        </Form.Item>

        <Form.Item label="PRICE ($)" name="price">
          <InputNumber size="large" min={0} step={0.01} />
        </Form.Item>

        <Form.Item label="SIZE" name="size">
          <Select
            mode="multiple"
            size="large"
            options={size}
            showArrow={true}
          />
        </Form.Item>

        <Form.Item label="COLOR" name="color">
          <Select
            mode="multiple"
            size="large"
            options={colors}
            showArrow={true}
            tagRender={(props) => (
              <Tag
                {...props}
                color={props.value === 'white' ? 'default' : props.value}
              >
                {props.label}
              </Tag>
            )}
          />
        </Form.Item>

        <Form.Item label="QUANTITY" name="quantity">
          <InputNumber size="large" min={0} step={1} />
        </Form.Item>

        <Form.Item label="DESCRIPTION" name="description">
          <Input.TextArea size="large" autoSize={{ minRows: 2 }} />
        </Form.Item>

        <Form.Item className="btn-group" wrapperCol={{ offset: 4, span: 16 }}>
          <Link to="/products">
            <Button
              type="default"
              className="secondary-btn"
              size="large"
              loading={loading}
            >
              Cancel
            </Button>
          </Link>
          <Button
            type="primary"
            className="primary-btn"
            size="large"
            htmlType="submit"
            loading={loading}
          >
            Submit
          </Button>
        </Form.Item>
      </Form>
    </Layout>
  );
}
