import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Form, Input, Layout, Typography } from 'antd';
import { Redirect } from 'react-router';

import './Login.scss';
import axios from '../../Util/axios';
import { login } from '../../store/userSlice';
import { setAuthHeader } from '../../Util/axios';

export default function Login() {
  const { user } = useSelector((state) => state.user);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  const LoginHandler = (form) => {
    setError(null);
    setLoading(true);
    axios
      .post('/admin/log-in', form)
      .then((res) => {
        setAuthHeader(res.data.token);
        dispatch(login(res.data));
      })
      .catch((err) => {
        console.log(err);
        const errorText = err.response
          ? err.response.data.message
          : "Something's wrong, please try again.";
        setError(errorText);
        setLoading(false);
      });
  };

  if (user) {
    return <Redirect to="/" />;
  }

  return (
    <Layout.Content className="login">
      <div className="login-container">
        <Typography.Title>Log in</Typography.Title>
        <Form name="login-form" onFinish={LoginHandler} layout="vertical">
          <Form.Item
            label="Email"
            name="email"
            validateTrigger={['onBlur']}
            validateStatus={!error ? 'validating' : 'error'}
            help={error}
            rules={[
              {
                type: 'email',
                message: 'Please enter a valid email!',
                required: true,
              },
            ]}
          >
            <Input size="large" placeholder="email@sample.com" />
          </Form.Item>

          <Form.Item
            rules={[
              {
                min: 6,
                max: 16,
                message: 'Password must be between 6 and 16 characters!',
                required: true,
              },
            ]}
            validateTrigger={['onBlur']}
            label="Password"
            name="password"
          >
            <Input.Password size="large" placeholder="Input password  " />
          </Form.Item>

          <Form.Item>
            <Button
              loading={loading}
              type="primary"
              htmlType="submit"
              block
              size="large"
            >
              Log in
            </Button>
          </Form.Item>
        </Form>
        <Button
          disabled={true}
          className="forgot"
          type="text"
          block
          size="large"
        >
          Forgot password?
        </Button>
      </div>
    </Layout.Content>
  );
}
