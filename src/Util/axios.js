import axios from 'axios';

const axiosConst = axios.create({
  baseURL: 'http://localhost:4808',
});

export const setAuthHeader = (token) => {
  axiosConst.defaults.headers.common['Authorization'] = `Bearer ${token}`;
};

export const clearAuthHeader = () => {
  delete axiosConst.defaults.headers.common['Authorization'];
};

export default axiosConst;
