import React from 'react';

import Layout from '../Layout';

export default function Main() {
  return (
    <Layout title="Main page">
      <h1>main 1 page</h1>
    </Layout>
  );
}
