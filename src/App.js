import { Switch, Route, Redirect } from 'react-router-dom';
import { Layout } from 'antd';

import './App.scss';
import Main from './Pages/Main/Main';
import Login from './Pages/Login/Login';
import Orders from './Pages/Orders/Orders';
import Products from './Pages/Products/Products';
import AddProduct from './Pages/AddProduct/AddProduct';
import EditProduct from './Pages/EditProduct/EditProduct';
import PrivateRoute from './Components/PrivateRoute/PrivateRoute';

function App() {
  return (
    <Layout>
      <Switch>
        <Route path="/log-in" component={Login} />
        <PrivateRoute path="/orders" component={Orders} />
        <PrivateRoute path="/products/add" component={AddProduct} />
        <PrivateRoute
          path="/products/edit/:productId"
          component={EditProduct}
        />
        <PrivateRoute path="/products" component={Products} />
        <PrivateRoute path="/overview" exact component={Main} />

        <Redirect to="/products" />
      </Switch>
    </Layout>
  );
}

export default App;
