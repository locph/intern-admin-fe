import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Avatar, Button, Dropdown, Menu } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import { logout } from '../../store/userSlice';

import './Header.scss';
import logoutIcon from '../../assets/images/logout.svg';

export default function Header(props) {
  const { user } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const menu = (
    <Menu>
      <Menu.Item
        icon={<img style={{ marginRight: 6 }} src={logoutIcon} alt="logout" />}
        key="logout"
        danger
        onClick={() => dispatch(logout())}
      >
        Đăng xuất
      </Menu.Item>
    </Menu>
  );
  return (
    <div className="header">
      <div className="title">
        <h1>{props.title}</h1>
        {!!props.subTitle && <h4>{props.subTitle}</h4>}
      </div>
      <div className="user">
        <Avatar size="large">{user.name[0]}</Avatar>
        <Dropdown overlay={menu} trigger={['click']}>
          <Button type="text" size="large">
            {user.name} <DownOutlined />
          </Button>
        </Dropdown>
      </div>
    </div>
  );
}
